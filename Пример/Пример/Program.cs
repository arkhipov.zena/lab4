﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Microsoft.VisualBasic;

namespace TimeTrees
{
    class Program
    {
        static (int, int, int) DeltaMinAndMaxDate(string[][] timeline)
        {
            (DateTime minDate, DateTime maxDate) = GetMinAndMaxDate(timeline);
            return (maxDate.Year - minDate.Year,
                maxDate.Month - minDate.Month,
                maxDate.Day - minDate.Day);
        }

        static (DateTime, DateTime) GetMinAndMaxDate(string[][] timeline)
        {
            DateTime minDate = DateTime.MaxValue, maxDate = DateTime.MinValue;
            foreach (var timeEvent in timeline)
            {
                DateTime date = ParseDate(timeEvent[0]);
                if (date < minDate) minDate = date;
                if (date > maxDate) maxDate = date;
            }

            return (minDate, maxDate);
        }

        static DateTime ParseDate(string value)
        {
            DateTime date;
            if (!DateTime.TryParseExact(value, "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
            {
                if (!DateTime.TryParseExact(value, "yyyy-mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    if (!DateTime.TryParseExact(value, "yyyy-mm-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                    {
                        throw new Exception("WRONG FORMAT");
                    }
                }
            }

            return date;
        }

        class MenuItem
        {
            public string Id;
            public string Text;
            public bool IsSelected;
        }

        static void DrawMenu(List<MenuItem> menu)
        {
            Console.Clear();
            foreach (MenuItem menuItem in menu)
            {
                Console.BackgroundColor = menuItem.IsSelected
                    ? ConsoleColor.DarkRed
                    : ConsoleColor.Black;

                Console.WriteLine(menuItem.Text);
            }

            Console.BackgroundColor = ConsoleColor.Black;
        }

        static void MenuSelectNext(List<MenuItem> menu)
        {
            var selectedItem = menu.First(x => x.IsSelected);
            int selectedIndex = menu.IndexOf(selectedItem);
            selectedItem.IsSelected = false;

            selectedIndex = selectedIndex == menu.Count - 1
                ? 0
                : ++selectedIndex;

            menu[selectedIndex].IsSelected = true;
        }

        static void MenuSelectPrev(List<MenuItem> menu)
        {
            var selectedItem = menu.First(x => x.IsSelected);
            int selectedIndex = menu.IndexOf(selectedItem);
            selectedItem.IsSelected = false;

            selectedIndex = selectedIndex == 0
                ? menu.Count - 1
                : --selectedIndex;

            menu[selectedIndex].IsSelected = true;
        }

        static void Menu()
        {
            Console.CursorVisible = false;
            List<MenuItem> menu = new List<MenuItem>
            {
                new MenuItem {Id = "start", Text = "Начать работу", IsSelected = true},
                new MenuItem {Id = "exit", Text = "Выход", IsSelected = false}
            };

            bool exit = false;
            do
            {
                DrawMenu(menu);

                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                switch (keyInfo.Key)
                {
                    case ConsoleKey.DownArrow:
                        MenuSelectNext(menu);
                        break;
                    case ConsoleKey.UpArrow:
                        MenuSelectPrev(menu);
                        break;
                    case ConsoleKey.Enter:
                        var selectedItem = menu.First(x => x.IsSelected);
                        Execute(selectedItem.Id);

                        Console.WriteLine("Хотите продолжить? y/n");
                        string answer = Console.ReadLine();
                        exit = answer == "n" || answer == "no";

                        break;
                }
            } while (!exit);
        }

        static void Execute(string commandId)
        {
            Console.Clear();
            switch (commandId)
            {
                case "start":
                    ExecuteProgram();
                    break;
            }
        }

        static void ExecuteProgram()
        {
            string timelineFile = Path.Combine(Environment.CurrentDirectory, "timeline.csv");
            string peopleFile = Path.Combine(Environment.CurrentDirectory, "people.csv");

            if (!File.Exists(timelineFile)
                || !File.Exists(peopleFile))
            {
                WriteTestFiles(peopleFile, timelineFile);
            }

            string[][] peopleData = ReadData(peopleFile);
            string[][] timelineData = ReadData(timelineFile);

            (int years, int months, int days) = DeltaMinAndMaxDate(timelineData);
            // void GetMinAndMaxDate(out DateTime min, out DateTime max);
            // Tuple<DateTime, DateTime> GetMinAndMaxDate(); new Tuple<DateTime, DateTime>>(dt1, dt2); var dates = GetMinAndMaxDate(); dates.Item1; dates.Item2;

            Console.WriteLine($"Между макс и мин датами прошло: {years} лет, {months} месяцев и {days} дней");
        }

        static void Main(string[] args)
        {
            Menu();
        }

        static string[][] ReadData(string path)
        {
            string[] data = File.ReadAllLines(path);
            string[][] splitData = new string[data.Length][];
            for (var i = 0; i < data.Length; i++)
            {
                var line = data[i];
                string[] parts = line.Split(";");
                splitData[i] = parts;
            }

            return splitData;
        }

        static string[] GenerateTimelineData()
        {
            return new[]
            {
                "1950;событие 1 бла-бла-бла",
                "1991-06-01;какое-то событие 2",
                "2000-01-01;наступил миллениум, ура-ура-ура"
            };
        }

        static string[] GeneratePeopleData()
        {
            return new[]
            {
                "1;Имя 1;2000-06-05",
                "2;Имя 2;1950-01-10;2010-01-01"
            };
        }

        static void WriteTestFiles(string peopleFile, string timelineFile)
        {
            File.WriteAllLines(peopleFile, GeneratePeopleData());
            File.WriteAllLines(timelineFile, GenerateTimelineData());
        }
    }
}